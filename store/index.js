export const state = () => ({
    users: [],
    posts: [],
    onePost:{},
    oneUser:'',
    photos:[]

})


export const getters = {
    getUsers: state => state.users,
    getPosts: state => state.posts,
    getOnePost: state => state.onePost,
    getOneUser: state => state.oneUser,
    getPhotos: state => state.photos,
}


export const mutations = {

    setUsersData(state, data) {
        state.users = data
    },
    setPostsData(state, data) {
        state.posts = data
    },
    setOnePost(state, data){
        state.onePost = data
    },
    setOneUser(state,data){
        state.oneUser = data
    },
    setPhotos(state, data){
        state.photos = data
    },
}


export const actions = {
    async getAllUsers(context) {
        await this.$axios
            .get(`/admin/users/get`)
            .then((response) => {
                context.commit('setUsersData', response.data.content)
            })
            .catch((e) => {
                console.log(e);
                // if (e.response.status === 422) {
                //     context.commit("setNotification", {
                //         text: e.response.data.error.message
                //     });
                // } else if (e.response.status === 400) {
                //     context.commit("setNotification", {
                //         text: "Что то пошло не так",
                //     });
                // } else {
                //     context.commit("setNotification", {
                //         text: "Не удалось получить список спикеров, пожалуйста попробуйте еще раз",
                //     });setUsersData
                // }
                // throw e;
            });
    },
    async usersToBan(context, data) {
        await this.$axios
            .patch(`/admin/user/ban`, data)
            .then((response) => {
                this.dispatch('getAllUsers')
            })
            .catch((e) => {
                console.log(e);
            });
    },
    async getAllPosts(context) {
        await this.$axios
            .get(`/admin/post/get?dir=ASC`)
            .then((response) => {
                context.commit('setPostsData', response.data.content)
            })
            .catch((e) => {
                console.log(e);
                // if (e.response.status === 422) {
                //     context.commit("setNotification", {
                //         text: e.response.data.error.message
                //     });
                // } else if (e.response.status === 400) {
                //     context.commit("setNotification", {
                //         text: "Что то пошло не так",
                //     });
                // } else {
                //     context.commit("setNotification", {
                //         text: "Не удалось получить список спикеров, пожалуйста попробуйте еще раз",
                //     });
                // }
                // throw e;
            });
    },
    async workWithPosts(context,data) {
        await this.$axios
            .patch(`/admin/post/change?status=${data.parameter}`,data.id)
            .then((response) => {
                this.dispatch('getAllPosts')
            })
            .catch((e) => {
                console.log(e);
                // if (e.response.status === 422) {
                //     context.commit("setNotification", {
                //         text: e.response.data.error.message
                //     });
                // } else if (e.response.status === 400) {
                //     context.commit("setNotification", {
                //         text: "Что то пошло не так",
                //     });
                // } else {
                //     context.commit("setNotification", {
                //         text: "Не удалось получить список спикеров, пожалуйста попробуйте еще раз",
                //     });
                // }
                // throw e;
            });
    },
    async getOnePost(context,data) {
        await this.$axios
            .get(`/admin/post/get?oid=${data}`)
            .then((response) => {
                context.commit('setOnePost', response.data.content[0])
            })
            .catch((e) => {
                console.log(e);
            });
    },
     async getPhotos(context,data) {
        await this.$axios
            .get(`/files/get/oid?file_oid=${data}`
            ,{
                responseType:"blob"
            }
            )
            .then((response) => {
                context.commit('setPhotos', response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    }
}

